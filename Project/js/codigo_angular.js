angular.module('myApp', [])
    .controller('ClienteController', ['$scope', '$http', ClienteController]);

function ClienteController($scope, $http) {

    $scope.cadastrar = function () {
        $http.post("https://reqres.in/api/users", $scope.cliente).then(function (success) {
            $scope.cliente = {};
            window.alert('Simula��o de cria��o de usu�rio concluida com sucesso!');
        });
    };

    $scope.consultar = function () {
        $http.get("https://reqres.in/api/users?page=2", ).then(function (model) {
            $scope.clientes = model.data.data;
        });
    };

    $scope.excluir = function (id) {
        $http.delete("https://reqres.in/api/users/" + id).then(function (success) {
            window.alert('Simula��o de usu�rio criada com sucesso!');
        });

    };

    $scope.detalhe = function (id) {
        $http.get("https://reqres.in/api/users/" + id).then(function (model) {
            $scope.detalhe = model.data.data;
        });
    }

    $scope.editar = function (objeto) {
        $http.put("https://reqres.in/api/users/" + objeto.id).then(function (model) {
            window.alert('Simula��o de altera��o de usu�rio efetuada com sucesso!');
        });

    }

}